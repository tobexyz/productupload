package de.schoenesnetz.productupload;



import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/")
@ApplicationScoped
@RegisterRestClient
public interface CSVService {
    @GET
    @Path("/articles/{lines}")
    @Produces("application/text")
    String getArticleList(@PathParam Integer lines);

    @PUT
    @Path("/products/{lines}")
    @Consumes("text/csv")
    String sendProductList(@PathParam Integer lines, String data);
    
}
