package de.schoenesnetz.productupload;

import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.minBy;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import io.quarkus.hibernate.validator.runtime.jaxrs.JaxrsEndPointValidated;

@Path("/api")
public class UploadResource {

    @Inject
    @RestClient
    CSVService csvService;

    @POST
    @Path("/convert/{lines}")
    @Produces(MediaType.TEXT_PLAIN)
    @JaxrsEndPointValidated            
    public String convert(@PathParam @Valid @Max(100000) Integer lines) {        
        String input = csvService.getArticleList(lines);            
        List<String> outputList = convert(Arrays.asList(input.split("\n")));                      
        String outputCSV =  outputList.stream().collect(
            StringBuilder::new, 
            (sb, l) -> sb.append(l).append("\n"),
            (sb, l) -> sb.append(l).append("\n")
        ).toString();   
        //kiss - passing result massage without any conversion to client 
        //encapsulate third partie messages in production     
        return csvService.sendProductList(lines, outputCSV);
        
    }

    protected List<String> convert(List<String> inputCSV){
        return convert( inputCSV.stream());
    }

    protected List<String> convert(Stream<String> inputCSV){
        List<String[]> articles =  inputCSV
        .filter( line -> !line.equals("id|produktId|name|beschreibung|preis|bestand"))
        .map(line -> line.split("[|]"))
        //Only articles with amount > 0
        .filter(lineArray -> Integer.parseInt( lineArray[5]) > 0).collect(toList());        
        //Count articles amount
        Map<String, Integer> amountOfProduct = articles.stream() 
             .collect( groupingBy(lineArray->lineArray[1],summingInt(lineArray->Integer.parseInt(lineArray[5]))));
        //find cheapest
        //preserve order
        Map<String, Optional<String[]>> cheapestArticleOfProduct = articles.stream().collect(
            groupingBy(lineArray->lineArray[1],LinkedHashMap::new, 
             minBy(comparingDouble(lineArray->Double.parseDouble(lineArray[4])))));    
        ArrayList<String> result = new ArrayList<String>();
        result.add("produktId|name|beschreibung|preis|summeBestand");
        cheapestArticleOfProduct.entrySet().stream().map(entry ->{ 
            if(entry.getValue().isPresent()){
                String[] item = entry.getValue().get();
                //format price
                item[4] = item[4].split("[.]")[1].length() == 2 ? item[4] : item[4]+"0";
                //set amount of all articles
                item[5]=""+amountOfProduct.get(entry.getKey());
                return Arrays.copyOfRange(item, 1, 6);
            }
            //should never be the case
            return new String[4];
        } ).map(l -> String.join("|", l)).forEachOrdered(s->result.add(s)); 
                return result;
        
    }
}