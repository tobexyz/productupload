package de.schoenesnetz.productupload;

import org.junit.jupiter.api.Test;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static java.util.stream.Collectors.*;


import java.util.List;
import java.util.stream.Stream;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConvertTest {
    
    @Test
    public void testConvertCSV() throws Exception{
        URI input = getClass().getClassLoader().getResource("products.csv").toURI();       
        URI expected = getClass().getClassLoader().getResource("expected/products_result.csv").toURI();       
        
		try (Stream<String> stream = Files.lines(Paths.get(input))) {           
            List<String> products = new UploadResource().convert(stream);           
             //products.stream().forEach(l -> System.out.println(l));
            try(Stream<String> expected_stream = Files.lines(Paths.get(expected))){
                assertEquals(expected_stream.collect(toList()), products);
            }
            
		} 
    }

    @Test
    public void testConvertCSV101() throws Exception{
        URI input = getClass().getClassLoader().getResource("products101.csv").toURI();       
        URI expected = getClass().getClassLoader().getResource("expected/products101_result.csv").toURI();       
        
		try (Stream<String> stream = Files.lines(Paths.get(input))) {           
            List<String> products = new UploadResource().convert(stream);           
             //products.stream().forEach(l -> System.out.println(l));
            try(Stream<String> expected_stream = Files.lines(Paths.get(expected))){
                assertEquals(expected_stream.collect(toList()), products);
            }
            
		} 
    }

    
    @Test
    public void testFaultyCSV() throws Exception{
        URI input = getClass().getClassLoader().getResource("products_faulty.csv").toURI();       
        
        
		try (Stream<String> stream = Files.lines(Paths.get(input))) {           
            try{
                new UploadResource().convert(stream);
                fail();
            }catch(Exception ex){
                //expected behaviour do nothing
            }
            
           
                  
		} 
    }

}
