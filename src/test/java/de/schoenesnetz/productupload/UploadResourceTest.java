package de.schoenesnetz.productupload;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

import org.junit.jupiter.api.Test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.stream.Stream;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * Testcases with mocked RESTClients
 */

@QuarkusTest
public class UploadResourceTest {

    @InjectMock
    @RestClient
    CSVService csvService;
    
    private String getTestArticles()throws Exception {
        return getResourceToString(getClass().getClassLoader().getResource("products.csv").toURI()); 
    }

    private String getTestProducts() throws Exception{
        return getResourceToString(getClass().getClassLoader().getResource("expected/products_result.csv").toURI());       
    }

    private String getResourceToString(URI resourceUri) throws Exception{
        
        try(Stream<String> stream = Files.lines(Paths.get(resourceUri))){
            return  stream.collect(
                StringBuilder::new, 
                (sb, l) -> sb.append(l).append("\n"),
                (sb, l) -> sb.append(l).append("\n")
            ).toString();           
        }        
    }

    @Test
    public void testConvertEndpoint() throws Exception{
        when(csvService.getArticleList(10)).thenReturn(getTestArticles());
        when(csvService.sendProductList(10, getTestProducts())).thenReturn("Finished reading data, result is correct");
        given()
          .pathParam("lines", 10)
          .when().post("/api/convert/{lines}")
          .then()
             .statusCode(200)
             .body(is("Finished reading data, result is correct"));
    }

    
}