package de.schoenesnetz.productupload;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.Test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

/**
 * Integrationtest only working with running coding-challenge-Server on localhost:8080
 */

@QuarkusTest
public class UploadResourceTestIT {

    

    @Test
    public void testConvertEndpointIT() throws Exception{        
        given()
          .pathParam("lines", 10)
          .when().post("/api/convert/{lines}")
          .then()
             .statusCode(200)
             .body(is("Finished reading data, result is correct"));
    }

    @Test
    public void testConvertEndpointBigIT() throws Exception{        
        given()
          .pathParam("lines", 10000)
          .when().post("/api/convert/{lines}")
          .then()
             .statusCode(200)
             .body(is("Finished reading data, result is correct"));
    }
}